package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositorySaveTest {

  @Test
  public void savedAccountCanBeFoundByItsId() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account();
    account.setId(13L);

    accountRepository.save(account);

    assertThat(accountRepository.findOne(13L))
        .isEqualTo(account);
  }

  @Test
  public void saveAccountWithIdHasChangedAttributesWhenFound() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account();
    account.deposit(15);
    account.setId(12L);
    accountRepository.save(account);

    Account found = accountRepository.findOne(12L);
    found.withdraw(7);
    accountRepository.save(found);

    Account account12 = accountRepository.findOne(12L);
    assertThat(account12.balance())
        .isEqualTo(8);
  }

  @Test
  public void newAccountGetsIdAssigned() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account();

    Account saved = accountRepository.save(account);

    assertThat(saved.getId())
        .isEqualTo(0L);
  }

  @Test
  public void savedAccountIsFoundById() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account = new Account();
    Account saved = accountRepository.save(account);

    assertThat(accountRepository.findOne(saved.getId()))
        .isEqualTo(saved);
  }

  @Test
  public void newlySavedAccountsHaveUniqueIds() {
    AccountRepository accountRepository = new FakeAccountRepository();
    Account account1 = new Account();
    account1 = accountRepository.save(account1);
    Account account2 = new Account();
    account2 = accountRepository.save(account2);

    assertThat(account1.getId())
        .isNotEqualTo(account2.getId());
  }

}
