package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountRepositoryFindTest {

  @Test
  public void newRepositoryIsEmpty() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();

    assertThat(accountRepository.findAll())
        .isEmpty();
  }

  @Test
  public void repositoryWithOneAccountIsReturnedInFindAll() throws Exception {
    Account account = new Account();
    AccountRepository accountRepository = new FakeAccountRepository();
    accountRepository.save(account);

    assertThat(accountRepository.findAll())
        .containsOnly(account);
  }

  @Test
  public void findAllShouldReturn2Accounts() {
    Account account1 = new Account();
    account1.setId(1L);
    Account account2 = new Account();
    account2.setId(2L);

    AccountRepository repo = new FakeAccountRepository();
    repo.save(account1);
    repo.save(account2);
    assertThat(repo.findAll())
        .hasSize(2)
        .containsOnly(account1, account2);
  }

  @Test
  public void findOneForEmptyRepositoryReturnsNull() throws Exception {
    AccountRepository accountRepository = new FakeAccountRepository();

    assertThat(accountRepository.findOne(7L))
        .isNull();
  }

  @Test
  public void findOneFindsAccountThatIsInRepository() throws Exception {
    Account account = new Account();
    account.setId(11L);
    AccountRepository accountRepository = new FakeAccountRepository();
    accountRepository.save(account);

    assertThat(accountRepository.findOne(11L))
        .isEqualTo(account);
  }

}
