package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.InsufficientBalanceException;
import com.visa.ncg.canteen.domain.InvalidAmountException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountWithdrawTest {

  @Test
  public void withdraw3DollarsFromAccountHaving7DollarsResultsIn4DollarBalance() throws Exception {
    Account account = new Account();
    account.deposit(7);

    account.withdraw(3);

    assertThat(account.balance())
        .isEqualTo(4);
  }

  @Test
  public void withdraw8And5From22IsBalanceOf9() throws Exception {
    Account account = new Account();
    account.deposit(22);

    account.withdraw(8);
    account.withdraw(5);

    assertThat(account.balance())
        .isEqualTo(9);
  }

  @Test
  public void withdraw0ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(0);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void withdrawNegativeThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.withdraw(-1);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void withdraw12FromBalanceOf11ThrowsInsufficientBalanceException() throws Exception {
    Account account = new Account();
    account.deposit(11);

    assertThatThrownBy(() -> {
      account.withdraw(12);
    })
        .isInstanceOf(InsufficientBalanceException.class);
  }
}
