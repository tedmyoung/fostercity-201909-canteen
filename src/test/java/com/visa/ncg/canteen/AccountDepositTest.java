package com.visa.ncg.canteen;

import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.InvalidAmountException;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountDepositTest {

  @Test
  public void newAccountHasZeroBalance() throws Exception {
    Account account = new Account();

    assertThat(account.balance())
        .isZero();
  }

  @Test
  public void deposit11ResultsInBalanceOf11() throws Exception {
    Account account = new Account();

    account.deposit(11);

    assertThat(account.balance()).
        isEqualTo(11);
  }

  @Test
  public void deposit7And5ResultsInBalanceOf12() throws Exception {
    Account account = new Account();

    account.deposit(7);
    account.deposit(5);

    assertThat(account.balance())
        .isEqualTo(12);
  }

  @Test
  public void deposit0ThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.deposit(0);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void depositNegativeThrowsInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatThrownBy(() -> {
      account.deposit(-1);
    })
        .isInstanceOf(InvalidAmountException.class);
  }
}
