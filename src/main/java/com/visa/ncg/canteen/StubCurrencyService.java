package com.visa.ncg.canteen;

import com.visa.ncg.canteen.adapter.currency.CurrencyService;
import org.springframework.stereotype.Service;

@Service
public class StubCurrencyService implements CurrencyService {
  @Override
  public int convertToGbp(int amount) {
    return 123;
  }

  @Override
  public int convertToJpy(int amount) {
    return 9999;
  }
}
