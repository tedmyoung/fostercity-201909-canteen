package com.visa.ncg.canteen.adapter.currency;

public class ConvertedCurrency {
  private String currency;
  private float converted;

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public float getConverted() {
    return converted;
  }

  public void setConverted(float converted) {
    this.converted = converted;
  }
}
