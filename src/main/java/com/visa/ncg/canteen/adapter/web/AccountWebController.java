package com.visa.ncg.canteen.adapter.web;

import com.visa.ncg.canteen.adapter.currency.CurrencyService;
import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class AccountWebController {

  private final AccountRepository accountRepository;
  private final CurrencyService currencyService;

  @Autowired
  public AccountWebController(AccountRepository accountRepository, CurrencyService currencyService) {
    this.accountRepository = accountRepository;
    this.currencyService = currencyService;
  }

  @GetMapping("/account/{id}")
  public String accountView(@PathVariable("id") String accountId, Model model) {
    Long id = Long.valueOf(accountId);

    Account account = accountRepository.findOne(id);
    if (account == null) {
      throw new NoSuchAccountHttpException();
    }

    AccountResponse accountResponse = AccountResponse.from(account);
    accountResponse.setGbpBalance(currencyService.convertToGbp(account.balance()));
    accountResponse.setJpyBalance(currencyService.convertToJpy(account.balance()));

    model.addAttribute("account", accountResponse);

    return "account-view";
  }

  @GetMapping("/account")
  public String allAccountsView(Model model) {
    List<Account> accounts = accountRepository.findAll();

    List<AccountResponse> responses = accounts
        .stream()
        .map(AccountResponse::from)
        .collect(Collectors.toList());

    model.addAttribute("accounts", responses);
    return "all-accounts";
  }

  @GetMapping("/create-account")
  public String createAccountForm(Model model) {
    CreateForm createForm = new CreateForm();
    createForm.setAccountName("");
    createForm.setInitialDeposit(100);

    model.addAttribute("createForm", createForm);
    return "create-account";
  }

  @PostMapping("/create-account")
  public String createAccount(CreateForm createForm) {
    Account account = new Account();
    account.changeNameTo(createForm.getAccountName());
    account.deposit(createForm.getInitialDeposit());

    Account saved = accountRepository.save(account);

    return "redirect:/account/" + saved.getId();
  }

}
