package com.visa.ncg.canteen.adapter.currency;

public interface CurrencyService {

  int convertToGbp(int amount);

  int convertToJpy(int amount);
}
