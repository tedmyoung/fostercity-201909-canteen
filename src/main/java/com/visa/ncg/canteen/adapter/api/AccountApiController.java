package com.visa.ncg.canteen.adapter.api;

import com.visa.ncg.canteen.adapter.web.AccountResponse;
import com.visa.ncg.canteen.domain.Account;
import com.visa.ncg.canteen.domain.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountApiController {

  private AccountRepository accountRepository;

  @Autowired
  public AccountApiController(AccountRepository accountRepository) {
    this.accountRepository = accountRepository;
  }

  @GetMapping("/api/accounts/{accountId}")
  public AccountResponse accountInfo(@PathVariable("accountId") String accountId) {
    Long id = Long.valueOf(accountId);

    Account account = accountRepository.findOne(id);

    return AccountResponse.from(account);
  }

  @PostMapping("/api/accounts")
  public AccountResponse createAccount(@RequestBody AccountCreateRequest request) {
    Account account = new Account();
    account.changeNameTo(request.getAccountName());
    account.deposit(request.getInitialBalance());

    Account saved = accountRepository.save(account);

    return AccountResponse.from(saved);
  }

}
