package com.visa.ncg.canteen.adapter.web;

import com.visa.ncg.canteen.domain.Account;

public class AccountResponse {
  private long id;
  private int balance;
  private String name;
  private int gbpBalance;
  private int jpyBalance;

  public static AccountResponse from(Account account) {

    AccountResponse accountResponse = new AccountResponse();

    accountResponse.setId(account.getId());
    accountResponse.setBalance(account.balance());
    accountResponse.setName(account.name());

    return accountResponse;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public int getBalance() {
    return balance;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }

  public void setGbpBalance(int gbpBalance) {
    this.gbpBalance = gbpBalance;
  }

  public int getGbpBalance() {
    return gbpBalance;
  }

  public void setJpyBalance(int jpyBalance) {
    this.jpyBalance = jpyBalance;
  }

  public int getJpyBalance() {
    return jpyBalance;
  }
}
